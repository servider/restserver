package com.servider.mobile.daos;

import com.servider.mobile.entities.Country;
import com.servider.mobile.entities.Sex;

/**
 *
 * @author Louis-Simon
 */
public class CountryDAO extends DAO<Country>{
    
    public CountryDAO() {
        super(Country.class);
    }
    
}
