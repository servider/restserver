package com.servider.mobile.daos;

import com.servider.mobile.entities.Sex;
import com.servider.mobile.entities.Verification;

/**
 *
 * @author Louis-Simon
 */
public class VerificationDAO extends DAO<Verification>{
    
    public VerificationDAO() {
        super(Verification.class);
    }
    
}
