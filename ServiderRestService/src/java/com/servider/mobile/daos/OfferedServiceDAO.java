package com.servider.mobile.daos;

import com.servider.mobile.entities.OfferedService;
import com.servider.mobile.entities.Sex;

/**
 *
 * @author Louis-Simon
 */
public class OfferedServiceDAO extends DAO<OfferedService>{
    
    public OfferedServiceDAO() {
        super(OfferedService.class);
    }
    
}
