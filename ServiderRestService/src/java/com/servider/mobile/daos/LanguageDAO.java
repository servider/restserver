package com.servider.mobile.daos;

import com.servider.mobile.entities.Language;
import com.servider.mobile.entities.Sex;

/**
 *
 * @author Louis-Simon
 */
public class LanguageDAO extends DAO<Language>{
    
    public LanguageDAO() {
        super(Language.class);
    }
    
}
