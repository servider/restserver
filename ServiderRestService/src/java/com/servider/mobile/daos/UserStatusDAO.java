package com.servider.mobile.daos;

import com.servider.mobile.entities.Sex;
import com.servider.mobile.entities.UserStatus;

/**
 *
 * @author Louis-Simon
 */
public class UserStatusDAO extends DAO<UserStatus>{
    
    public UserStatusDAO() {
        super(UserStatus.class);
    }
    
}
