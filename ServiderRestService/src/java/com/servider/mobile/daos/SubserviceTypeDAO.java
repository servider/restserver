package com.servider.mobile.daos;

import com.servider.mobile.entities.Sex;
import com.servider.mobile.entities.SubserviceType;

/**
 *
 * @author Louis-Simon
 */
public class SubserviceTypeDAO extends DAO<SubserviceType>{
    
    public SubserviceTypeDAO() {
        super(SubserviceType.class);
    }
    
}
