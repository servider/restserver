package com.servider.mobile.daos;

import com.servider.mobile.entities.Contract;
import com.servider.mobile.entities.Sex;

/**
 *
 * @author Louis-Simon
 */
public class ContractDAO extends DAO<Contract>{
    
    public ContractDAO() {
        super(Contract.class);
    }
    
}
