package com.servider.mobile.daos;

import com.servider.mobile.entities.Comment;
import com.servider.mobile.entities.Sex;

/**
 *
 * @author Louis-Simon
 */
public class CommentDAO extends DAO<Comment>{
    
    public CommentDAO() {
        super(Comment.class);
    }
    
}
