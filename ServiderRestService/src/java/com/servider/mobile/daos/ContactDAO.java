package com.servider.mobile.daos;

import com.servider.mobile.entities.Contact;
import com.servider.mobile.entities.Sex;

/**
 *
 * @author Louis-Simon
 */
public class ContactDAO extends DAO<Contact>{
    
    public ContactDAO() {
        super(Contact.class);
    }
    
}
