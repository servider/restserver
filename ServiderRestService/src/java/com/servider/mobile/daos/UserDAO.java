package com.servider.mobile.daos;

import com.servider.mobile.entities.User;
import javax.ejb.Stateless;

/**
 *
 * @author Louis-Simon
 */
@Stateless
public class UserDAO extends DAO<User> {

    public UserDAO() {
        super(User.class);
    }
}
