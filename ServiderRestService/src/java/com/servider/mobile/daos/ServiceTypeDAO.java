package com.servider.mobile.daos;

import com.servider.mobile.entities.ServiceType;
import com.servider.mobile.entities.Sex;

/**
 *
 * @author Louis-Simon
 */
public class ServiceTypeDAO extends DAO<ServiceType>{
    
    public ServiceTypeDAO() {
        super(ServiceType.class);
    }
    
}
