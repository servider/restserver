package com.servider.mobile.daos;

import com.servider.mobile.entities.Message;
import com.servider.mobile.entities.Sex;

/**
 *
 * @author Louis-Simon
 */
public class MessageDAO extends DAO<Message>{
    
    public MessageDAO() {
        super(Message.class);
    }
    
}
