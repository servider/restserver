package com.servider.mobile.daos;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 *
 * @author Louis-Simon
 * @param <T>
 */
public abstract class DAO<T> {

    @PersistenceContext(name = "persistence/em")
    private EntityManager em;

    private final Class<T> entityClass;

    protected DAO(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected EntityManager getEntityManager() {
        return em == null ? initializeEm() : em;
    }

    private EntityManager initializeEm() {
        try {
            InitialContext initialContext = new InitialContext();
            em = (EntityManager) initialContext.lookup("java:comp/env/persistence/em");
        } catch (NamingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return em;
    }

    private ConstraintViolationException validationConstraintsInfractionsDetected(T entity) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(entity);
        if (constraintViolations.size() > 0) {
            Iterator<ConstraintViolation<T>> iterator = constraintViolations.iterator();
            while (iterator.hasNext()) {
                ConstraintViolation<T> cv = iterator.next();
                System.err.println(cv.getRootBeanClass().getName() + "."
                        + cv.getPropertyPath() + " " + cv.getMessage());
            }
            return new ConstraintViolationException(constraintViolations);
        } else {
            return null;
        }
    }

    public void create(T entity) throws ConstraintViolationException {
        ConstraintViolationException violations
                = validationConstraintsInfractionsDetected(entity);
        if (violations == null) {
            getEntityManager().persist(entity);
        } else {
            throw violations;
        }
    }

    public void edit(T entity) throws ConstraintViolationException {
        ConstraintViolationException violations
                = validationConstraintsInfractionsDetected(entity);
        if (violations == null) {
            getEntityManager().merge(entity);
        } else {
            throw violations;
        }
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq
                = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public T findConditions(Map<String, String> conditions) {
        CriteriaBuilder qb = em.getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<T> customer = cq.from(entityClass);
        List<Predicate> predicates = new ArrayList<>();

        for (Map.Entry<String, String> condition : conditions.entrySet()) {
            predicates.add(qb.equal(customer.get(condition.getKey()), condition.getValue()));
        }

        cq.select(customer).where(predicates.toArray(new Predicate[]{}));
        return (T) em.createQuery(cq).getSingleResult();
    }

    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq
                = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq
                = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}
