package com.servider.mobile.daos;

import com.servider.mobile.entities.PricingType;
import com.servider.mobile.entities.Sex;

/**
 *
 * @author Louis-Simon
 */
public class PricingTypeDAO extends DAO<PricingType>{
    
    public PricingTypeDAO() {
        super(PricingType.class);
    }
    
}
