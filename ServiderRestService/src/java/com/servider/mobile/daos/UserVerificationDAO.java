package com.servider.mobile.daos;

import com.servider.mobile.entities.Sex;
import com.servider.mobile.entities.UserVerification;

/**
 *
 * @author Louis-Simon
 */
public class UserVerificationDAO extends DAO<UserVerification>{
    
    public UserVerificationDAO() {
        super(UserVerification.class);
    }
    
}
