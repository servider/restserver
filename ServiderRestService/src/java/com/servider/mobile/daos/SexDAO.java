package com.servider.mobile.daos;

import com.servider.mobile.entities.Sex;

/**
 *
 * @author Louis-Simon
 */
public class SexDAO extends DAO<Sex>{
    
    public SexDAO() {
        super(Sex.class);
    }
    
}
