package com.servider.mobile.daos;

import com.servider.mobile.entities.Offer;
import com.servider.mobile.entities.Sex;

/**
 *
 * @author Louis-Simon
 */
public class OfferDAO extends DAO<Offer>{
    
    public OfferDAO() {
        super(Offer.class);
    }
    
}
