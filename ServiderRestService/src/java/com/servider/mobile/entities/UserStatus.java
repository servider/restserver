package com.servider.mobile.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Louis-Simon
 */
@Entity
@Table(name = "user_status")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserStatus.findAll", query = "SELECT u FROM UserStatus u"),
    @NamedQuery(name = "UserStatus.findByIdUserStatus", query = "SELECT u FROM UserStatus u WHERE u.idUserStatus = :idUserStatus"),
    @NamedQuery(name = "UserStatus.findByName", query = "SELECT u FROM UserStatus u WHERE u.name = :name")})
public class UserStatus implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userStatusId")
    private Collection<User> userCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_user_status")
    private Integer idUserStatus;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;

    public UserStatus() {
    }

    public UserStatus(Integer idUserStatus) {
        this.idUserStatus = idUserStatus;
    }

    public UserStatus(Integer idUserStatus, String name) {
        this.idUserStatus = idUserStatus;
        this.name = name;
    }

    public Integer getIdUserStatus() {
        return idUserStatus;
    }

    public void setIdUserStatus(Integer idUserStatus) {
        this.idUserStatus = idUserStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUserStatus != null ? idUserStatus.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserStatus)) {
            return false;
        }
        UserStatus other = (UserStatus) object;
        if ((this.idUserStatus == null && other.idUserStatus != null) || (this.idUserStatus != null && !this.idUserStatus.equals(other.idUserStatus))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.servider.mobile.entities.UserStatus[ idUserStatus=" + idUserStatus + " ]";
    }

    @XmlTransient
    public Collection<User> getUserCollection() {
        return userCollection;
    }

    public void setUserCollection(Collection<User> userCollection) {
        this.userCollection = userCollection;
    }
}
