/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servider.mobile.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Louis-Simon
 */
@Entity
@Table(name = "user_verification")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserVerification.findAll", query = "SELECT u FROM UserVerification u"),
    @NamedQuery(name = "UserVerification.findByIdUserVerification", query = "SELECT u FROM UserVerification u WHERE u.idUserVerification = :idUserVerification")})
public class UserVerification implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_user_verification")
    private Integer idUserVerification;
    @JoinColumn(name = "user_id", referencedColumnName = "id_user")
    @ManyToOne(optional = false)
    private User userId;
    @JoinColumn(name = "verification_id", referencedColumnName = "id_verification")
    @ManyToOne(optional = false)
    private Verification verificationId;

    public UserVerification() {
    }

    public UserVerification(Integer idUserVerification) {
        this.idUserVerification = idUserVerification;
    }

    public Integer getIdUserVerification() {
        return idUserVerification;
    }

    public void setIdUserVerification(Integer idUserVerification) {
        this.idUserVerification = idUserVerification;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Verification getVerificationId() {
        return verificationId;
    }

    public void setVerificationId(Verification verificationId) {
        this.verificationId = verificationId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUserVerification != null ? idUserVerification.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserVerification)) {
            return false;
        }
        UserVerification other = (UserVerification) object;
        if ((this.idUserVerification == null && other.idUserVerification != null) || (this.idUserVerification != null && !this.idUserVerification.equals(other.idUserVerification))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.servider.mobile.entities.UserVerification[ idUserVerification=" + idUserVerification + " ]";
    }
    
}
