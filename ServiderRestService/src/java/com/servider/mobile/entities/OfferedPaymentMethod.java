package com.servider.mobile.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Louis-Simon
 */
@Entity
@Table(name = "offered_payment_method")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OfferedPaymentMethod.findAll", query = "SELECT o FROM OfferedPaymentMethod o"),
    @NamedQuery(name = "OfferedPaymentMethod.findByIdOfferedPaymentMethod", query = "SELECT o FROM OfferedPaymentMethod o WHERE o.idOfferedPaymentMethod = :idOfferedPaymentMethod")})
public class OfferedPaymentMethod implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_offered_payment_method")
    private Integer idOfferedPaymentMethod;
    @JoinColumn(name = "payment_method_id_payment_method", referencedColumnName = "id_payment_method")
    @ManyToOne(optional = false)
    private PaymentMethod paymentMethodIdPaymentMethod;
    @JoinColumn(name = "offered_service_id_service", referencedColumnName = "id_service")
    @ManyToOne(optional = false)
    private OfferedService offeredServiceIdService;

    public OfferedPaymentMethod() {
    }

    public OfferedPaymentMethod(Integer idOfferedPaymentMethod) {
        this.idOfferedPaymentMethod = idOfferedPaymentMethod;
    }

    public Integer getIdOfferedPaymentMethod() {
        return idOfferedPaymentMethod;
    }

    public void setIdOfferedPaymentMethod(Integer idOfferedPaymentMethod) {
        this.idOfferedPaymentMethod = idOfferedPaymentMethod;
    }

    public PaymentMethod getPaymentMethodIdPaymentMethod() {
        return paymentMethodIdPaymentMethod;
    }

    public void setPaymentMethodIdPaymentMethod(PaymentMethod paymentMethodIdPaymentMethod) {
        this.paymentMethodIdPaymentMethod = paymentMethodIdPaymentMethod;
    }

    public OfferedService getOfferedServiceIdService() {
        return offeredServiceIdService;
    }

    public void setOfferedServiceIdService(OfferedService offeredServiceIdService) {
        this.offeredServiceIdService = offeredServiceIdService;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idOfferedPaymentMethod != null ? idOfferedPaymentMethod.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OfferedPaymentMethod)) {
            return false;
        }
        OfferedPaymentMethod other = (OfferedPaymentMethod) object;
        if ((this.idOfferedPaymentMethod == null && other.idOfferedPaymentMethod != null) || (this.idOfferedPaymentMethod != null && !this.idOfferedPaymentMethod.equals(other.idOfferedPaymentMethod))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.servider.mobile.entities.OfferedPaymentMethod[ idOfferedPaymentMethod=" + idOfferedPaymentMethod + " ]";
    }

}
