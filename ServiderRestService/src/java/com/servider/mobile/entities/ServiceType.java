/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servider.mobile.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Louis-Simon
 */
@Entity
@Table(name = "service_type")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ServiceType.findAll", query = "SELECT s FROM ServiceType s"),
    @NamedQuery(name = "ServiceType.findByIdTypeService", query = "SELECT s FROM ServiceType s WHERE s.idTypeService = :idTypeService"),
    @NamedQuery(name = "ServiceType.findByName", query = "SELECT s FROM ServiceType s WHERE s.name = :name")})
public class ServiceType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_Type_Service")
    private Integer idTypeService;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    @JoinColumn(name = "pricing_id", referencedColumnName = "id_pricing_type")
    @ManyToOne(optional = false)
    private PricingType pricingId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "typeServiceId")
    private Collection<OfferedService> offeredServiceCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "typeServiceid")
    private Collection<SubserviceType> subserviceTypeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "typeServiceId")
    private Collection<Comment> commentCollection;

    public ServiceType() {
    }

    public ServiceType(Integer idTypeService) {
        this.idTypeService = idTypeService;
    }

    public ServiceType(Integer idTypeService, String name) {
        this.idTypeService = idTypeService;
        this.name = name;
    }

    public Integer getIdTypeService() {
        return idTypeService;
    }

    public void setIdTypeService(Integer idTypeService) {
        this.idTypeService = idTypeService;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PricingType getPricingId() {
        return pricingId;
    }

    public void setPricingId(PricingType pricingId) {
        this.pricingId = pricingId;
    }

    @XmlTransient
    public Collection<OfferedService> getOfferedServiceCollection() {
        return offeredServiceCollection;
    }

    public void setOfferedServiceCollection(Collection<OfferedService> offeredServiceCollection) {
        this.offeredServiceCollection = offeredServiceCollection;
    }

    @XmlTransient
    public Collection<SubserviceType> getSubserviceTypeCollection() {
        return subserviceTypeCollection;
    }

    public void setSubserviceTypeCollection(Collection<SubserviceType> subserviceTypeCollection) {
        this.subserviceTypeCollection = subserviceTypeCollection;
    }

    @XmlTransient
    public Collection<Comment> getCommentCollection() {
        return commentCollection;
    }

    public void setCommentCollection(Collection<Comment> commentCollection) {
        this.commentCollection = commentCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTypeService != null ? idTypeService.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServiceType)) {
            return false;
        }
        ServiceType other = (ServiceType) object;
        if ((this.idTypeService == null && other.idTypeService != null) || (this.idTypeService != null && !this.idTypeService.equals(other.idTypeService))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.servider.mobile.entities.ServiceType[ idTypeService=" + idTypeService + " ]";
    }
    
}
