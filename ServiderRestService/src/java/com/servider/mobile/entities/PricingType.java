/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servider.mobile.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Louis-Simon
 */
@Entity
@Table(name = "pricing_type")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PricingType.findAll", query = "SELECT p FROM PricingType p"),
    @NamedQuery(name = "PricingType.findByIdPricingType", query = "SELECT p FROM PricingType p WHERE p.idPricingType = :idPricingType"),
    @NamedQuery(name = "PricingType.findByName", query = "SELECT p FROM PricingType p WHERE p.name = :name")})
public class PricingType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_pricing_type")
    private Integer idPricingType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pricingId")
    private Collection<ServiceType> serviceTypeCollection;

    public PricingType() {
    }

    public PricingType(Integer idPricingType) {
        this.idPricingType = idPricingType;
    }

    public PricingType(Integer idPricingType, String name) {
        this.idPricingType = idPricingType;
        this.name = name;
    }

    public Integer getIdPricingType() {
        return idPricingType;
    }

    public void setIdPricingType(Integer idPricingType) {
        this.idPricingType = idPricingType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<ServiceType> getServiceTypeCollection() {
        return serviceTypeCollection;
    }

    public void setServiceTypeCollection(Collection<ServiceType> serviceTypeCollection) {
        this.serviceTypeCollection = serviceTypeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPricingType != null ? idPricingType.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PricingType)) {
            return false;
        }
        PricingType other = (PricingType) object;
        if ((this.idPricingType == null && other.idPricingType != null) || (this.idPricingType != null && !this.idPricingType.equals(other.idPricingType))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.servider.mobile.entities.PricingType[ idPricingType=" + idPricingType + " ]";
    }
    
}
