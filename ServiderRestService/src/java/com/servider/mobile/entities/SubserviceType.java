/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servider.mobile.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Louis-Simon
 */
@Entity
@Table(name = "subservice_type")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SubserviceType.findAll", query = "SELECT s FROM SubserviceType s"),
    @NamedQuery(name = "SubserviceType.findByIdSubService", query = "SELECT s FROM SubserviceType s WHERE s.idSubService = :idSubService"),
    @NamedQuery(name = "SubserviceType.findByName", query = "SELECT s FROM SubserviceType s WHERE s.name = :name")})
public class SubserviceType implements Serializable {

    @JoinColumn(name = "service_type_id", referencedColumnName = "id_Type_Service")
    @ManyToOne(optional = false)
    private ServiceType serviceTypeId;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_sub_service")
    private Integer idSubService;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "subserviceId")
    private Collection<Offer> offerCollection;
    @JoinColumn(name = "Type_Service_id", referencedColumnName = "id_Type_Service")
    @ManyToOne(optional = false)
    private ServiceType typeServiceid;

    public SubserviceType() {
    }

    public SubserviceType(Integer idSubService) {
        this.idSubService = idSubService;
    }

    public SubserviceType(Integer idSubService, String name) {
        this.idSubService = idSubService;
        this.name = name;
    }

    public Integer getIdSubService() {
        return idSubService;
    }

    public void setIdSubService(Integer idSubService) {
        this.idSubService = idSubService;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<Offer> getOfferCollection() {
        return offerCollection;
    }

    public void setOfferCollection(Collection<Offer> offerCollection) {
        this.offerCollection = offerCollection;
    }

    public ServiceType getTypeServiceid() {
        return typeServiceid;
    }

    public void setTypeServiceid(ServiceType typeServiceid) {
        this.typeServiceid = typeServiceid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSubService != null ? idSubService.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SubserviceType)) {
            return false;
        }
        SubserviceType other = (SubserviceType) object;
        if ((this.idSubService == null && other.idSubService != null) || (this.idSubService != null && !this.idSubService.equals(other.idSubService))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.servider.mobile.entities.SubserviceType[ idSubService=" + idSubService + " ]";
    }

    public ServiceType getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(ServiceType serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }
    
}
