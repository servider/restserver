package com.servider.mobile.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Louis-Simon
 */
@Entity
@Table(name = "sex")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sex.findAll", query = "SELECT s FROM Sex s"),
    @NamedQuery(name = "Sex.findByIdSex", query = "SELECT s FROM Sex s WHERE s.idSex = :idSex"),
    @NamedQuery(name = "Sex.findByName", query = "SELECT s FROM Sex s WHERE s.name = :name")})
public class Sex implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sexId")
    private Collection<User> userCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_sex")
    private Integer idSex;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;

    public Sex() {
    }

    public Sex(Integer idSex) {
        this.idSex = idSex;
    }

    public Sex(Integer idSex, String name) {
        this.idSex = idSex;
        this.name = name;
    }

    public Integer getIdSex() {
        return idSex;
    }

    public void setIdSex(Integer idSex) {
        this.idSex = idSex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSex != null ? idSex.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sex)) {
            return false;
        }
        Sex other = (Sex) object;
        if ((this.idSex == null && other.idSex != null) || (this.idSex != null && !this.idSex.equals(other.idSex))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.servider.mobile.entities.Sex[ idSex=" + idSex + " ]";
    }

    @XmlTransient
    public Collection<User> getUserCollection() {
        return userCollection;
    }

    public void setUserCollection(Collection<User> userCollection) {
        this.userCollection = userCollection;
    }
    
}
