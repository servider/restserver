/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servider.mobile.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Louis-Simon
 */
@Entity
@Table(name = "offered_service")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OfferedService.findAll", query = "SELECT o FROM OfferedService o"),
    @NamedQuery(name = "OfferedService.findByIdService", query = "SELECT o FROM OfferedService o WHERE o.idService = :idService"),
    @NamedQuery(name = "OfferedService.findByActif", query = "SELECT o FROM OfferedService o WHERE o.actif = :actif")})
public class OfferedService implements Serializable {

    @Column(name = "insert_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "offeredServiceId")
    private Collection<OfferedLanguage> offeredLanguageCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "offeredServiceIdService")
    private Collection<OfferedPaymentMethod> offeredPaymentMethodCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_service")
    private Integer idService;
    @Basic(optional = false)
    @NotNull
    @Column(name = "actif")
    private boolean actif;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "serviceId")
    private Collection<Offer> offerCollection;
    @JoinColumn(name = "utilisateur_id", referencedColumnName = "id_user")
    @ManyToOne(optional = false)
    private User utilisateurId;
    @JoinColumn(name = "typeService_Id", referencedColumnName = "id_Type_Service")
    @ManyToOne(optional = false)
    private ServiceType typeServiceId;

    public OfferedService() {
    }

    public OfferedService(Integer idService) {
        this.idService = idService;
    }

    public OfferedService(Integer idService, boolean actif) {
        this.idService = idService;
        this.actif = actif;
    }

    public Integer getIdService() {
        return idService;
    }

    public void setIdService(Integer idService) {
        this.idService = idService;
    }

    public boolean getActif() {
        return actif;
    }

    public void setActif(boolean actif) {
        this.actif = actif;
    }

    @XmlTransient
    public Collection<Offer> getOfferCollection() {
        return offerCollection;
    }

    public void setOfferCollection(Collection<Offer> offerCollection) {
        this.offerCollection = offerCollection;
    }

    public User getUtilisateurId() {
        return utilisateurId;
    }

    public void setUtilisateurId(User utilisateurId) {
        this.utilisateurId = utilisateurId;
    }

    public ServiceType getTypeServiceId() {
        return typeServiceId;
    }

    public void setTypeServiceId(ServiceType typeServiceId) {
        this.typeServiceId = typeServiceId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idService != null ? idService.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OfferedService)) {
            return false;
        }
        OfferedService other = (OfferedService) object;
        if ((this.idService == null && other.idService != null) || (this.idService != null && !this.idService.equals(other.idService))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.servider.mobile.entities.OfferedService[ idService=" + idService + " ]";
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    @XmlTransient
    public Collection<OfferedLanguage> getOfferedLanguageCollection() {
        return offeredLanguageCollection;
    }

    public void setOfferedLanguageCollection(Collection<OfferedLanguage> offeredLanguageCollection) {
        this.offeredLanguageCollection = offeredLanguageCollection;
    }

    @XmlTransient
    public Collection<OfferedPaymentMethod> getOfferedPaymentMethodCollection() {
        return offeredPaymentMethodCollection;
    }

    public void setOfferedPaymentMethodCollection(Collection<OfferedPaymentMethod> offeredPaymentMethodCollection) {
        this.offeredPaymentMethodCollection = offeredPaymentMethodCollection;
    }
    
}
