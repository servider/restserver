/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servider.mobile.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Louis-Simon
 */
@Entity
@Table(name = "verification")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Verification.findAll", query = "SELECT v FROM Verification v"),
    @NamedQuery(name = "Verification.findByIdVerification", query = "SELECT v FROM Verification v WHERE v.idVerification = :idVerification"),
    @NamedQuery(name = "Verification.findByName", query = "SELECT v FROM Verification v WHERE v.name = :name")})
public class Verification implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_verification")
    private Integer idVerification;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "verificationId")
    private Collection<UserVerification> userVerificationCollection;

    public Verification() {
    }

    public Verification(Integer idVerification) {
        this.idVerification = idVerification;
    }

    public Verification(Integer idVerification, String name) {
        this.idVerification = idVerification;
        this.name = name;
    }

    public Integer getIdVerification() {
        return idVerification;
    }

    public void setIdVerification(Integer idVerification) {
        this.idVerification = idVerification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<UserVerification> getUserVerificationCollection() {
        return userVerificationCollection;
    }

    public void setUserVerificationCollection(Collection<UserVerification> userVerificationCollection) {
        this.userVerificationCollection = userVerificationCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVerification != null ? idVerification.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Verification)) {
            return false;
        }
        Verification other = (Verification) object;
        if ((this.idVerification == null && other.idVerification != null) || (this.idVerification != null && !this.idVerification.equals(other.idVerification))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.servider.mobile.entities.Verification[ idVerification=" + idVerification + " ]";
    }
    
}
