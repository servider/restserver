package com.servider.mobile.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Louis-Simon
 */
@Entity
@Table(name = "offered_language")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OfferedLanguage.findAll", query = "SELECT o FROM OfferedLanguage o"),
    @NamedQuery(name = "OfferedLanguage.findByIdOfferedLanguage", query = "SELECT o FROM OfferedLanguage o WHERE o.idOfferedLanguage = :idOfferedLanguage")})
public class OfferedLanguage implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_offered_language")
    private Integer idOfferedLanguage;
    @JoinColumn(name = "offered_service_id", referencedColumnName = "id_service")
    @ManyToOne(optional = false)
    private OfferedService offeredServiceId;
    @JoinColumn(name = "language_d_language", referencedColumnName = "id_language")
    @ManyToOne(optional = false)
    private Language languageDLanguage;

    public OfferedLanguage() {
    }

    public OfferedLanguage(Integer idOfferedLanguage) {
        this.idOfferedLanguage = idOfferedLanguage;
    }

    public Integer getIdOfferedLanguage() {
        return idOfferedLanguage;
    }

    public void setIdOfferedLanguage(Integer idOfferedLanguage) {
        this.idOfferedLanguage = idOfferedLanguage;
    }

    public OfferedService getOfferedServiceId() {
        return offeredServiceId;
    }

    public void setOfferedServiceId(OfferedService offeredServiceId) {
        this.offeredServiceId = offeredServiceId;
    }

    public Language getLanguageDLanguage() {
        return languageDLanguage;
    }

    public void setLanguageDLanguage(Language languageDLanguage) {
        this.languageDLanguage = languageDLanguage;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idOfferedLanguage != null ? idOfferedLanguage.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OfferedLanguage)) {
            return false;
        }
        OfferedLanguage other = (OfferedLanguage) object;
        if ((this.idOfferedLanguage == null && other.idOfferedLanguage != null) || (this.idOfferedLanguage != null && !this.idOfferedLanguage.equals(other.idOfferedLanguage))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.servider.mobile.entities.OfferedLanguage[ idOfferedLanguage=" + idOfferedLanguage + " ]";
    }

}
