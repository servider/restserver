/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servider.mobile.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Louis-Simon
 */
@Entity
@Table(name = "comment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Comment.findAll", query = "SELECT c FROM Comment c"),
    @NamedQuery(name = "Comment.findByIdComment", query = "SELECT c FROM Comment c WHERE c.idComment = :idComment"),
    @NamedQuery(name = "Comment.findByCommentedClientOrService", query = "SELECT c FROM Comment c WHERE c.commentedClientOrService = :commentedClientOrService"),
    @NamedQuery(name = "Comment.findByEvaluationCriteria1", query = "SELECT c FROM Comment c WHERE c.evaluationCriteria1 = :evaluationCriteria1"),
    @NamedQuery(name = "Comment.findByEvaluationCriteria2", query = "SELECT c FROM Comment c WHERE c.evaluationCriteria2 = :evaluationCriteria2"),
    @NamedQuery(name = "Comment.findByEvaluationCriteria3", query = "SELECT c FROM Comment c WHERE c.evaluationCriteria3 = :evaluationCriteria3"),
    @NamedQuery(name = "Comment.findByEvaluationAvg", query = "SELECT c FROM Comment c WHERE c.evaluationAvg = :evaluationAvg"),
    @NamedQuery(name = "Comment.findByRecommended", query = "SELECT c FROM Comment c WHERE c.recommended = :recommended"),
    @NamedQuery(name = "Comment.findByCommentDate", query = "SELECT c FROM Comment c WHERE c.commentDate = :commentDate")})
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_comment")
    private Integer idComment;
    @Basic(optional = false)
    @NotNull
    @Column(name = "commented_client_or_service")
    private boolean commentedClientOrService;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 2147483647)
    @Column(name = "comment")
    private String comment;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "response")
    private String response;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "private_comment")
    private String privateComment;
    @Basic(optional = false)
    @NotNull
    @Column(name = "evaluation_criteria_1")
    private int evaluationCriteria1;
    @Basic(optional = false)
    @NotNull
    @Column(name = "evaluation_criteria_2")
    private int evaluationCriteria2;
    @Basic(optional = false)
    @NotNull
    @Column(name = "evaluation_criteria_3")
    private int evaluationCriteria3;
    @Basic(optional = false)
    @NotNull
    @Column(name = "evaluation_avg")
    private double evaluationAvg;
    @Basic(optional = false)
    @NotNull
    @Column(name = "recommended")
    private boolean recommended;
    @Basic(optional = false)
    @NotNull
    @Column(name = "comment_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date commentDate;
    @OneToMany(mappedBy = "clientcommentId")
    private Collection<Contract> contractCollection;
    @OneToMany(mappedBy = "servidercommentId")
    private Collection<Contract> contractCollection1;
    @JoinColumn(name = "commenter_id", referencedColumnName = "id_user")
    @ManyToOne(optional = false)
    private User commenterId;
    @JoinColumn(name = "commented_id", referencedColumnName = "id_user")
    @ManyToOne(optional = false)
    private User commentedId;
    @JoinColumn(name = "type_service_id", referencedColumnName = "id_Type_Service")
    @ManyToOne(optional = false)
    private ServiceType typeServiceId;

    public Comment() {
    }

    public Comment(Integer idComment) {
        this.idComment = idComment;
    }

    public Comment(Integer idComment, boolean commentedClientOrService, String comment, int evaluationCriteria1, int evaluationCriteria2, int evaluationCriteria3, double evaluationAvg, boolean recommended, Date commentDate) {
        this.idComment = idComment;
        this.commentedClientOrService = commentedClientOrService;
        this.comment = comment;
        this.evaluationCriteria1 = evaluationCriteria1;
        this.evaluationCriteria2 = evaluationCriteria2;
        this.evaluationCriteria3 = evaluationCriteria3;
        this.evaluationAvg = evaluationAvg;
        this.recommended = recommended;
        this.commentDate = commentDate;
    }

    public Integer getIdComment() {
        return idComment;
    }

    public void setIdComment(Integer idComment) {
        this.idComment = idComment;
    }

    public boolean getCommentedClientOrService() {
        return commentedClientOrService;
    }

    public void setCommentedClientOrService(boolean commentedClientOrService) {
        this.commentedClientOrService = commentedClientOrService;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getPrivateComment() {
        return privateComment;
    }

    public void setPrivateComment(String privateComment) {
        this.privateComment = privateComment;
    }

    public int getEvaluationCriteria1() {
        return evaluationCriteria1;
    }

    public void setEvaluationCriteria1(int evaluationCriteria1) {
        this.evaluationCriteria1 = evaluationCriteria1;
    }

    public int getEvaluationCriteria2() {
        return evaluationCriteria2;
    }

    public void setEvaluationCriteria2(int evaluationCriteria2) {
        this.evaluationCriteria2 = evaluationCriteria2;
    }

    public int getEvaluationCriteria3() {
        return evaluationCriteria3;
    }

    public void setEvaluationCriteria3(int evaluationCriteria3) {
        this.evaluationCriteria3 = evaluationCriteria3;
    }

    public double getEvaluationAvg() {
        return evaluationAvg;
    }

    public void setEvaluationAvg(double evaluationAvg) {
        this.evaluationAvg = evaluationAvg;
    }

    public boolean getRecommended() {
        return recommended;
    }

    public void setRecommended(boolean recommended) {
        this.recommended = recommended;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    @XmlTransient
    public Collection<Contract> getContractCollection() {
        return contractCollection;
    }

    public void setContractCollection(Collection<Contract> contractCollection) {
        this.contractCollection = contractCollection;
    }

    @XmlTransient
    public Collection<Contract> getContractCollection1() {
        return contractCollection1;
    }

    public void setContractCollection1(Collection<Contract> contractCollection1) {
        this.contractCollection1 = contractCollection1;
    }

    public User getCommenterId() {
        return commenterId;
    }

    public void setCommenterId(User commenterId) {
        this.commenterId = commenterId;
    }

    public User getCommentedId() {
        return commentedId;
    }

    public void setCommentedId(User commentedId) {
        this.commentedId = commentedId;
    }

    public ServiceType getTypeServiceId() {
        return typeServiceId;
    }

    public void setTypeServiceId(ServiceType typeServiceId) {
        this.typeServiceId = typeServiceId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idComment != null ? idComment.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Comment)) {
            return false;
        }
        Comment other = (Comment) object;
        if ((this.idComment == null && other.idComment != null) || (this.idComment != null && !this.idComment.equals(other.idComment))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.servider.mobile.entities.Comment[ idComment=" + idComment + " ]";
    }
    
}
