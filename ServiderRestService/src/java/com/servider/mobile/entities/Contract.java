package com.servider.mobile.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Louis-Simon
 */
@Entity
@Table(name = "contract")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contract.findAll", query = "SELECT c FROM Contract c"),
    @NamedQuery(name = "Contract.findByIdContract", query = "SELECT c FROM Contract c WHERE c.idContract = :idContract"),
    @NamedQuery(name = "Contract.findByEstimatedPrice", query = "SELECT c FROM Contract c WHERE c.estimatedPrice = :estimatedPrice"),
    @NamedQuery(name = "Contract.findByDate", query = "SELECT c FROM Contract c WHERE c.date = :date")})
public class Contract implements Serializable {

    @Column(name = "insert_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date insertDate;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_Contract")
    private Integer idContract;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "estimated_price")
    private Double estimatedPrice;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @JoinColumn(name = "client_Id", referencedColumnName = "id_user")
    @ManyToOne(optional = false)
    private User clientId;
    @JoinColumn(name = "offer_Id", referencedColumnName = "id_Offer")
    @ManyToOne(optional = false)
    private Offer offerId;
    @JoinColumn(name = "client_comment_Id", referencedColumnName = "id_comment")
    @ManyToOne
    private Comment clientcommentId;
    @JoinColumn(name = "servider_comment_Id", referencedColumnName = "id_comment")
    @ManyToOne
    private Comment servidercommentId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contractId")
    private Collection<Message> messageCollection;

    public Contract() {
    }

    public Contract(Integer idContract) {
        this.idContract = idContract;
    }

    public Integer getIdContract() {
        return idContract;
    }

    public void setIdContract(Integer idContract) {
        this.idContract = idContract;
    }

    public Double getEstimatedPrice() {
        return estimatedPrice;
    }

    public void setEstimatedPrice(Double estimatedPrice) {
        this.estimatedPrice = estimatedPrice;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getClientId() {
        return clientId;
    }

    public void setClientId(User clientId) {
        this.clientId = clientId;
    }

    public Offer getOfferId() {
        return offerId;
    }

    public void setOfferId(Offer offerId) {
        this.offerId = offerId;
    }

    public Comment getClientcommentId() {
        return clientcommentId;
    }

    public void setClientcommentId(Comment clientcommentId) {
        this.clientcommentId = clientcommentId;
    }

    public Comment getServidercommentId() {
        return servidercommentId;
    }

    public void setServidercommentId(Comment servidercommentId) {
        this.servidercommentId = servidercommentId;
    }

    @XmlTransient
    public Collection<Message> getMessageCollection() {
        return messageCollection;
    }

    public void setMessageCollection(Collection<Message> messageCollection) {
        this.messageCollection = messageCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idContract != null ? idContract.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contract)) {
            return false;
        }
        Contract other = (Contract) object;
        if ((this.idContract == null && other.idContract != null) || (this.idContract != null && !this.idContract.equals(other.idContract))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.servider.mobile.entities.Contract[ idContract=" + idContract + " ]";
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }
    
}
