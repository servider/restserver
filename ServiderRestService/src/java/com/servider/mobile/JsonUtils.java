package com.servider.mobile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Louis-Simon
 */
public final class JsonUtils {

    private static final String ERROR_STRING = "Json parsing error";
    private static final Object ERROR_OBJECT = null;

    private static ObjectMapper objectMapper;
    private static ObjectWriter objectWriter;

    private JsonUtils() {
        throw new AssertionError();
    }

    static {
        initializeObjects();
    }

    private static void initializeObjects() {
        objectMapper = new ObjectMapper();
        objectWriter = objectMapper.writer().withDefaultPrettyPrinter();
    }

    public static String toJson(Object object) throws JsonProcessingException {
        String json = ERROR_STRING;
        try {
            json = objectWriter.writeValueAsString(object);
        } catch (JsonProcessingException ex) {
            initializeObjects();
            Logger.getLogger(JsonUtils.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }

        return json;
    }

    public static <T> T fromJson(String json, Class<T> type) throws IOException {
        Object object = ERROR_OBJECT;
        try {
            object = (T) objectMapper.readValue(json, type);
        } catch (IOException ex) {
            initializeObjects();
            Logger.getLogger(JsonUtils.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
        return (T) object;
    }

}
