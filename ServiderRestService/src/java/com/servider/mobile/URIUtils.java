package com.servider.mobile;

import java.net.URI;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Louis-Simon
 */
public final class URIUtils {
    
    private URIUtils(){
    }
    
    public static URI getURIFromContextAndId(UriInfo context, int id){
        UriBuilder builder = context.getAbsolutePathBuilder();
        return builder.path(Integer.toString(id)).build();
    }
}
