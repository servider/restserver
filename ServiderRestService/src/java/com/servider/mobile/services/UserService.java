package com.servider.mobile.services;

import com.servider.mobile.entities.Country;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.servider.mobile.JsonUtils;
import com.servider.mobile.URIUtils;
import com.servider.mobile.daos.UserDAO;
import com.servider.mobile.entities.Language;
import com.servider.mobile.entities.Sex;
import com.servider.mobile.entities.User;
import com.servider.mobile.entities.UserStatus;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.persistence.NoResultException;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriBuilder;

/**
 * REST Web Service
 *
 * @author Louis-Simon
 */
@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserService {

    @Context
    private UriInfo context;

    public UserService() {
    }

    @EJB
    private UserDAO userDAO;

    @PostConstruct
    public void init() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJson() {
        ResponseBuilder builder;
        try {
            builder = Response.ok(JsonUtils.toJson(userDAO.findAll()));
        } catch (Exception ex) {
            builder = Response.serverError();
        }

        return builder.build();
    }

    @GET
    @Path("/conditions")
    @Produces(MediaType.APPLICATION_JSON)
    public Response checkCredentials(@QueryParam("email") String email,
            @QueryParam("password") String password) {
        ResponseBuilder builder;
        try {
            HashMap<String, String> hashConditions = new HashMap<>();
            hashConditions.put("email", email);
            hashConditions.put("password", password);
            builder = Response.ok(JsonUtils.toJson(userDAO.findConditions(hashConditions)));
        } catch (EJBException ex) {
            builder = Response.noContent();
        } catch (Exception ex) {
            builder = Response.serverError();
        }

        return builder.build();
    }

    @GET
    @Path("/all")
    public Response getAllJson() {
        ResponseBuilder builder;
        try {
            builder = Response.ok(JsonUtils.toJson(userDAO.findAll()));
        } catch (Exception ex) {
            builder = Response.serverError();
        }

        return builder.build();
    }

    @DELETE
    @Path("/{content}")
    public Response deleteJson(@PathParam("content") String content) {
        ResponseBuilder builder;
        try {
            userDAO.remove(JsonUtils.fromJson(content, User.class));
            builder = Response.noContent();
        } catch (IOException e) {
            builder = Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE);
        } catch (Exception e) {
            builder = Response.serverError();
        }

        return builder.build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postJson(String content) {
        ResponseBuilder builder;
        try {
            User user = JsonUtils.fromJson(content, User.class);
            userDAO.create(user);
            builder = Response.ok(URIUtils.getURIFromContextAndId(context, user.getIdUser()));
        } catch (ConstraintViolationException e) {
            builder = Response.status(Response.Status.BAD_REQUEST);
        } catch (IOException e) {
            builder = Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE);
        } catch (Exception e) {
            builder = Response.serverError();
        }

        return builder.build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putJson(String content) {
        System.out.println(content);
        ResponseBuilder builder;
        try {
            User user = JsonUtils.fromJson(content, User.class);
            userDAO.edit(user);
            builder = Response.ok(URIUtils.getURIFromContextAndId(context, user.getIdUser()));
        } catch (ConstraintViolationException e) {
            builder = Response.status(Response.Status.BAD_REQUEST);
        } catch (IOException e) {
            builder = Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE);
        } catch (Exception e) {
            builder = Response.serverError();
        }

        return builder.build();
    }
}
