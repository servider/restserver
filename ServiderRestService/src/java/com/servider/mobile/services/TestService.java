package com.servider.mobile.services;

import com.servider.mobile.entities.Country;
import com.servider.mobile.JsonUtils;
import com.servider.mobile.daos.UserDAO;
import com.servider.mobile.entities.Language;
import com.servider.mobile.entities.Sex;
import com.servider.mobile.entities.User;
import com.servider.mobile.entities.UserStatus;
import java.io.IOException;
import java.net.URI;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriBuilder;

/**
 * REST Web Service
 *
 * @author Louis-Simon
 */
@Path("/test")
@Produces(MediaType.APPLICATION_JSON)
public class TestService {

    @Context
    private UriInfo context;

    public TestService() {
    }

    @EJB
    private UserDAO userDAO;

    @PostConstruct
    public void init() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJson() {
        ResponseBuilder builder;
//        HashMap<String,String> map = new HashMap<>();
//        map.put("email", "email@domain.com");
//        map.put("password","Password");
//        try {
//            System.out.println(JsonUtils.toJson(map));
//        } catch (JsonProcessingException ex) {
//            System.out.println("failed to json the map");
//        }
        try {
            builder = Response.ok(JsonUtils.toJson(userDAO.findAll()));
        } catch (Exception ex) {
            builder = Response.serverError();
        }

        return builder.build();
    }

    private User makeTestUser() {
        User user = new User();
        Language language = new Language();
        Sex sex = new Sex();
        UserStatus userStatus = new UserStatus();
        Country country = new Country();

        language.setName("french");
        language.setIdLanguage(1);

        sex.setName("male");
        sex.setIdSex(1);

        userStatus.setName("client");
        userStatus.setIdUserStatus(1);

        country.setIdCountry(21);
        country.setCurrencyName("Canada");
        country.setPhonePrefix("1");
        country.setCurrencyName("Dollars");
        country.setCurrencySymbol("$");

        user.setLanguageId(language);
        user.setSexId(sex);
        user.setUserStatusId(userStatus);
        user.setCountryId(country);
        user.setAddress("address");
        user.setCity("city");
        user.setCompanyName("company name");
        user.setCountry("country");
        user.setEmail("email@domain.com");
        user.setFacebookAccount("12345");
        user.setGivenName("Given Name");
        user.setIdUser(4);
        user.setIsServider(Boolean.TRUE);
        user.setLastConnectionDate(null);
        user.setLat(50);
        user.setLng(50);
        user.setName("Name");
        user.setPassword("Password");
        user.setPhone("Phone");
        user.setPostalCode("Postal Code");
        user.setProfileDescription("Description");
        user.setProfileImgPath("c:/some/path");
        return user;
    }

    @DELETE
    @Path("/{content}")
    public Response deleteJson(@PathParam("content") String content) {
        ResponseBuilder builder;
        try {
            userDAO.remove(JsonUtils.fromJson(content, User.class));
            builder = Response.noContent();
        } catch (IOException e) {
            builder = Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE);
        } catch (Exception e) {
            builder = Response.serverError();
        }

        return builder.build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postJson(String content) {
        ResponseBuilder builder;
        try {
            User user = JsonUtils.fromJson(content, User.class);
            userDAO.create(user);
            builder = Response.ok(getUserURI(user.getIdUser()));
        } catch (ConstraintViolationException e) {
            builder = Response.status(Response.Status.BAD_REQUEST);
        } catch (IOException e) {
            builder = Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE);
        } catch (Exception e) {
            builder = Response.serverError();
        }

        return builder.build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putJson(String content) {
        System.out.println(content);
        ResponseBuilder builder;
        try {
            User user = JsonUtils.fromJson(content, User.class);
            userDAO.edit(user);
            builder = Response.ok(getUserURI(user.getIdUser()));
        } catch (ConstraintViolationException e) {
            builder = Response.status(Response.Status.BAD_REQUEST);
        } catch (IOException e) {
            builder = Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE);
        } catch (Exception e) {
            builder = Response.serverError();
        }

        return builder.build();
    }

    private URI getUserURI(int userId) {
        UriBuilder builder = context.getAbsolutePathBuilder();
        return builder.path(Integer.toString(userId)).build();
    }
}
