package com.servider.mobile.entities;

import com.servider.mobile.entities.ServiceType;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-02-10T11:33:26")
@StaticMetamodel(PricingType.class)
public class PricingType_ { 

    public static volatile CollectionAttribute<PricingType, ServiceType> serviceTypeCollection;
    public static volatile SingularAttribute<PricingType, String> name;
    public static volatile SingularAttribute<PricingType, Integer> idPricingType;

}