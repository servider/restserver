package com.servider.mobile.entities;

import com.servider.mobile.entities.Contract;
import com.servider.mobile.entities.ServiceType;
import com.servider.mobile.entities.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-02-10T11:33:26")
@StaticMetamodel(Comment.class)
public class Comment_ { 

    public static volatile SingularAttribute<Comment, User> commenterId;
    public static volatile SingularAttribute<Comment, ServiceType> typeServiceId;
    public static volatile SingularAttribute<Comment, Date> commentDate;
    public static volatile SingularAttribute<Comment, User> commentedId;
    public static volatile SingularAttribute<Comment, Boolean> commentedClientOrService;
    public static volatile CollectionAttribute<Comment, Contract> contractCollection;
    public static volatile SingularAttribute<Comment, Boolean> recommended;
    public static volatile CollectionAttribute<Comment, Contract> contractCollection1;
    public static volatile SingularAttribute<Comment, Double> evaluationAvg;
    public static volatile SingularAttribute<Comment, String> response;
    public static volatile SingularAttribute<Comment, Integer> evaluationCriteria2;
    public static volatile SingularAttribute<Comment, Integer> evaluationCriteria1;
    public static volatile SingularAttribute<Comment, Integer> evaluationCriteria3;
    public static volatile SingularAttribute<Comment, String> privateComment;
    public static volatile SingularAttribute<Comment, String> comment;
    public static volatile SingularAttribute<Comment, Integer> idComment;

}