package com.servider.mobile.entities;

import com.servider.mobile.entities.Offer;
import com.servider.mobile.entities.ServiceType;
import com.servider.mobile.entities.User;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-02-10T11:33:26")
@StaticMetamodel(OfferedService.class)
public class OfferedService_ { 

    public static volatile CollectionAttribute<OfferedService, Offer> offerCollection;
    public static volatile SingularAttribute<OfferedService, ServiceType> typeServiceId;
    public static volatile SingularAttribute<OfferedService, Integer> idService;
    public static volatile SingularAttribute<OfferedService, Boolean> actif;
    public static volatile SingularAttribute<OfferedService, User> utilisateurId;

}