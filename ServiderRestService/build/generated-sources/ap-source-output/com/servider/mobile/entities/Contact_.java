package com.servider.mobile.entities;

import com.servider.mobile.entities.User;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-02-10T11:33:26")
@StaticMetamodel(Contact.class)
public class Contact_ { 

    public static volatile SingularAttribute<Contact, Integer> idContact;
    public static volatile SingularAttribute<Contact, User> contactId;
    public static volatile SingularAttribute<Contact, User> userId;
    public static volatile SingularAttribute<Contact, Boolean> isCustomerOrServider;

}