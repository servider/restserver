package com.servider.mobile.entities;

import com.servider.mobile.entities.Language;
import com.servider.mobile.entities.Sex;
import com.servider.mobile.entities.UserStatus;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-02-10T11:33:26")
@StaticMetamodel(User.class)
public class User_ { 

    public static volatile SingularAttribute<User, Date> lastConnectionDate;
    public static volatile SingularAttribute<User, String> phone;
    public static volatile SingularAttribute<User, String> facebookAccount;
    public static volatile SingularAttribute<User, Integer> lng;
    public static volatile SingularAttribute<User, String> profileDescription;
    public static volatile SingularAttribute<User, String> givenName;
    public static volatile SingularAttribute<User, String> companyName;
    public static volatile SingularAttribute<User, Language> languageId;
    public static volatile SingularAttribute<User, UserStatus> userStatusId;
    public static volatile SingularAttribute<User, String> password;
    public static volatile SingularAttribute<User, String> city;
    public static volatile SingularAttribute<User, String> country;
    public static volatile SingularAttribute<User, String> profileImgPath;
    public static volatile SingularAttribute<User, String> postalCode;
    public static volatile SingularAttribute<User, String> address;
    public static volatile SingularAttribute<User, String> email;
    public static volatile SingularAttribute<User, Sex> sexId;
    public static volatile SingularAttribute<User, String> name;
    public static volatile SingularAttribute<User, Integer> idUser;
    public static volatile SingularAttribute<User, Boolean> isServider;
    public static volatile SingularAttribute<User, Integer> lat;

}