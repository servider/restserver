package com.servider.mobile.entities;

import com.servider.mobile.entities.User;
import com.servider.mobile.entities.Verification;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-02-10T11:33:26")
@StaticMetamodel(UserVerification.class)
public class UserVerification_ { 

    public static volatile SingularAttribute<UserVerification, User> userId;
    public static volatile SingularAttribute<UserVerification, Integer> idUserVerification;
    public static volatile SingularAttribute<UserVerification, Verification> verificationId;

}