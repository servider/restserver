package com.servider.mobile.entities;

import com.servider.mobile.entities.Contract;
import com.servider.mobile.entities.OfferedService;
import com.servider.mobile.entities.SubserviceType;
import com.servider.mobile.entities.User;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-02-10T11:33:26")
@StaticMetamodel(Offer.class)
public class Offer_ { 

    public static volatile SingularAttribute<Offer, String> pricing;
    public static volatile SingularAttribute<Offer, OfferedService> serviceId;
    public static volatile SingularAttribute<Offer, String> address;
    public static volatile SingularAttribute<Offer, String> description;
    public static volatile SingularAttribute<Offer, Integer> idOffer;
    public static volatile SingularAttribute<Offer, User> userId;
    public static volatile SingularAttribute<Offer, Boolean> active;
    public static volatile CollectionAttribute<Offer, Contract> contractCollection;
    public static volatile SingularAttribute<Offer, SubserviceType> subserviceId;

}