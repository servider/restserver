package com.servider.mobile.entities;

import com.servider.mobile.entities.Offer;
import com.servider.mobile.entities.ServiceType;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-02-10T11:33:26")
@StaticMetamodel(SubserviceType.class)
public class SubserviceType_ { 

    public static volatile SingularAttribute<SubserviceType, Integer> idSubService;
    public static volatile CollectionAttribute<SubserviceType, Offer> offerCollection;
    public static volatile SingularAttribute<SubserviceType, ServiceType> typeServiceid;
    public static volatile SingularAttribute<SubserviceType, String> name;

}