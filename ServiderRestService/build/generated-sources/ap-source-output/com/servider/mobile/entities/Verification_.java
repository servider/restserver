package com.servider.mobile.entities;

import com.servider.mobile.entities.UserVerification;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-02-10T11:33:26")
@StaticMetamodel(Verification.class)
public class Verification_ { 

    public static volatile SingularAttribute<Verification, String> name;
    public static volatile CollectionAttribute<Verification, UserVerification> userVerificationCollection;
    public static volatile SingularAttribute<Verification, Integer> idVerification;

}