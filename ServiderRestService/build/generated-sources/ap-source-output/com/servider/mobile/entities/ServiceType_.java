package com.servider.mobile.entities;

import com.servider.mobile.entities.Comment;
import com.servider.mobile.entities.OfferedService;
import com.servider.mobile.entities.PricingType;
import com.servider.mobile.entities.SubserviceType;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-02-10T11:33:26")
@StaticMetamodel(ServiceType.class)
public class ServiceType_ { 

    public static volatile CollectionAttribute<ServiceType, Comment> commentCollection;
    public static volatile SingularAttribute<ServiceType, String> name;
    public static volatile CollectionAttribute<ServiceType, SubserviceType> subserviceTypeCollection;
    public static volatile SingularAttribute<ServiceType, Integer> idTypeService;
    public static volatile SingularAttribute<ServiceType, PricingType> pricingId;
    public static volatile CollectionAttribute<ServiceType, OfferedService> offeredServiceCollection;

}