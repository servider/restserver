package com.servider.mobile.entities;

import com.servider.mobile.entities.Contract;
import com.servider.mobile.entities.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-02-10T11:33:26")
@StaticMetamodel(Message.class)
public class Message_ { 

    public static volatile SingularAttribute<Message, String> content;
    public static volatile SingularAttribute<Message, Date> sendDate;
    public static volatile SingularAttribute<Message, Integer> idMessage;
    public static volatile SingularAttribute<Message, User> fromId;
    public static volatile SingularAttribute<Message, Boolean> visible;
    public static volatile SingularAttribute<Message, User> toId;
    public static volatile SingularAttribute<Message, Contract> contractId;

}