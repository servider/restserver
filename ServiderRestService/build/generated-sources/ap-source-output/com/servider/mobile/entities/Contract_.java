package com.servider.mobile.entities;

import com.servider.mobile.entities.Comment;
import com.servider.mobile.entities.Message;
import com.servider.mobile.entities.Offer;
import com.servider.mobile.entities.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-02-10T11:33:26")
@StaticMetamodel(Contract.class)
public class Contract_ { 

    public static volatile SingularAttribute<Contract, Comment> clientcommentId;
    public static volatile SingularAttribute<Contract, Comment> servidercommentId;
    public static volatile SingularAttribute<Contract, Double> estimatedPrice;
    public static volatile CollectionAttribute<Contract, Message> messageCollection;
    public static volatile SingularAttribute<Contract, Integer> idContract;
    public static volatile SingularAttribute<Contract, Date> date;
    public static volatile SingularAttribute<Contract, User> clientId;
    public static volatile SingularAttribute<Contract, Offer> offerId;

}